# TableMakerforCSV
Table maker for CSV: A module for creating HTML tables from CSV files.

## Installation
Download mod_tablemakerforcsv[x].zip file and install it in Joomla. If you download release file, you should extract it to find mod_tablemakerforcsv[x].zip

## Donate to support us                                          
                                                                   
[![Donate with Bitcoin](https://en.cryptobadges.io/badge/small/16f1DStB3YG3R4BMTa1zGYRxN9i7FAqtUX)](https://en.cryptobadges.io/donate/16f1DStB3YG3R4BMTa1zGYRxN9i7FAqtUX)
                                                   
  [![Donate with Bitcoin](https://en.cryptobadges.io/badge/big/16f1DStB3YG3R4BMTa1zGYRxN9i7FAqtUX)](https://en.cryptobadges.io/donate/16f1DStB3YG3R4BMTa1zGYRxN9i7FAqtUX)
